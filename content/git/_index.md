+++
title = "Git"
description = ""
+++

## Branches

Usamos a branch _master_ como _staging_ do projeto e a branch _production_ para o código que está em produção.
Quando for necessário criar alguma _feature_ ou resolver algum _bug_ do sistema recomendamos criar a _branch_ sempre a partir da _master_.

{{% notice info %}}
A branch _production_ sempre é bloqueada no projeto. É apenas atualizada com _pull requests_ por ser a branch com o código em produção
{{% /notice %}}


O padrão para criação de braches usado é:

`feature/`: usado quando se irá implementar alguma parte da aplicação.

`fix/`: usado quando se quer arrumar alguma parte da aplicação que está quebrada.

`chore/`: usado quando se quer adicionar algum código que não chega a ser uma parte da aplicação, como uma refatoração.

## Commit

Mensagens de commit devem fazer sentido para quem for lê-las depois. Descrever claramente na mensagem de _commit_ sobre o que foi feito.
Não quer dizer que se deve commitar sempre após editar algum arquivo. O objetivo do _commit_ é poder dividir o que vai ser feito na branch, em pequenas tarefas, melhorando o versionamento.

{{% notice note %}}
Saiba como escrever uma boa mensagem de commit [neste link](https://chris.beams.io/posts/git-commit/).
{{% /notice %}}

## Gitignore

O arquivo `.gitignore` é usado para declarar o que você deseja que não vá para seu repositório.

## Pull Request (PR) ou Merge Request (MR)

_Pull requests_ devem ser feitos quando o objetivo proposto quando você abriu aquela _branch_ terminou. Por exemplo, se você terminou alguma _feature_ ou corrigiu o _bug_ pelo qual essa branch teve que ser aberta.

Na maior parte do tempo os _pull requests_ (PR's) vão para a branch _master_. _PR_ para branch _production_ apenas quando for subir o código que está na _master_ para _production_, ou seja, subir o código para produção.

Na descrição de um PR, alguns repositórios dão a opção de usar um _template_ feito pelo time para cumprir uma lista de itens para que o PR esteja pronto pra ser revisado. Sempre verifique se o repositório contém esse _template_, e se tiver, use-o.

Caso nao haja algum tipo de _template_ tente especificar do melhor jeito possível o que foi feito na branch, para que o _reviewer_ possa entender melhor o que procurar quando estiver revisando o PR.

{{% notice info %}}
Um PR só pode ser mergeado se **UM ou MAIS** _reviewers_ aprovarem o PR.
{{% /notice %}}

## Code Review

O _code review_ acontece quando um PR é aberto. O programador que abriu o PR seleciona um outro membro do _squad_ ou projeto que possa fazer essa revisão e aguarda sua aprovação para poder mergear a _branch_ em questão.

## Reviewers

São os programadores que revisam os PR's. 
O trabalho de um _reviewer_ é garantir que o código faça sentido, siga os padrões do projeto, cumpra as especificações esperadas pelo PR.

## Comandos e Alias úteis

Adicione esses comandos ao seu `bashrc`.

```bash
git-current-branch () {
    if ! git rev-parse 2> /dev/null
    then
        print "$0: not a repository: $PWD" >&2
        return 1
    fi
    local ref="$(git symbolic-ref HEAD 2> /dev/null)"
    if [[ -n "$ref" ]]
    then
        print "${ref#refs/heads/}"
        return 0
    else
        return 1
    fi
}

grmb () {
    if [[ $# = 0 ]]
    then
        git branch --merged master | grep -v master | xargs -n 1 git branch -d
    else
        git branch --merged $1 | grep -v $1 | xargs -n 1 git branch -d
    fi
}

alias vim="nvim" #open nvim by default when type vim
alias g="git"
alias gc="git commit"
alias gst="git status"
alias gco="git checkout"
alias gp='git push origin "$(git-current-branch 2> /dev/null)"' #git push to te current branch
alias gpull='git pull origin "$(git-current-branch 2> /dev/null)" --rebase' #git pull the current branch with rebase flag active
alias gaa="git add --all"
alias grh="git reset --hard"
alias gdb="git branch -D"
alias glog='git log --pretty=oneline --abbrev-commit --graph --decorate' #beautify git log with commit hash
alias gdm=grmb #delete all your branchs fully merged with master or with your branch passed as param
alias gbl="git branch -l" #list all your branchs
```
