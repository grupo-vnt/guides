+++
title = "Cargos"
description = ""
weight = 1
+++

## VNT

### Conselho

TBD

### Diretoria

A diretoria é compartilhada com as empresas do grupo.

* CEO: [Matheus Danemberg](https://www.linkedin.com/in/mdanemberg/)
* CTO: [Victor Antoniazzi](https://www.linkedin.com/in/vgsantoniazzi/)
* COO: [Felipe Adamoli](https://www.linkedin.com/in/felipeadamoli/)

## Nave

* IT Manager: [João Pedro Bretanha](https://www.linkedin.com/in/jpbretanha/)
* Especialistas:
  * [Gustavo Pinho](https://www.linkedin.com/in/gustavo-pinho-908ba2173/)
  * [Juliano Reis](https://www.linkedin.com/in/juliano-reis-2ab203142/)
  * [Caio Silva](https://www.linkedin.com/in/caio-silva-606567161/)
  * [Antônio Paracy](https://www.linkedin.com/in/antonioparacy/)

## Thrive

TBD